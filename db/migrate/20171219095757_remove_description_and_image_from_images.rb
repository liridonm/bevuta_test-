class RemoveDescriptionAndImageFromImages < ActiveRecord::Migration[5.1]
  def change
    remove_column :images, :description, :text
    remove_column :images, :image, :string
  end
end

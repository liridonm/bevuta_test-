class Image < ApplicationRecord
  after_create :set_width_height
  validates_presence_of :url
  validate :check_dimensions_by_url


  def check_dimensions
    if !image_cache.nil? && (image.width >= 500 || image.height >= 500)
      errors.add :image, "Dimension too big."
    end
  end

  def check_dimensions_by_url
    image = MiniMagick::Image.open(self.url)
    if image.width >= 500 || image.height >= 500
      errors.add :url, 'Dimensions are to big!'
    end
  end

  private

  def set_width_height
    width_height = MiniMagick::Image.open(self.url)

    self.width = width_height.width
    self.height = width_height.height
    self.image_type = width_height.type
    self.save
  end

end

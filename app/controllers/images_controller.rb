class ImagesController < ApplicationController

  def index
    @images = Image.all.reverse
    unless @images.blank?
    @images.each do |i|
      @image_id = i.id
      @image_image_type = i.image_type
      @image_description = i.url
    end
    end
  end

  def new
    @image = Image.new

  end

  def create
    @image = Image.create(image_params)
    file = MiniMagick::Image.open("#{@image.url}")
    save_to_local = file.write("public/assets/image_#{@image.id}.#{file.type}")
    if @image.save
      save_to_local
      redirect_to root_path, notice: 'Image is successfully uploaded'
    else
      render :new
    end

  end

  def show
    @image = Image.find(params[:id])
  end

  def edit
    @image = Image.find(params[:id])
    @m = MiniMagick::Image.open("public/assets/image_#{@image.id}.#{@image.image_type}")
    @width =  @m.width
    @height = @m.height
  end

  def update
    @image = Image.find(params[:id])
    file = MiniMagick::Image.open("public/assets/image_#{@image.id}.#{@image.image_type}")
    @width = file.width
    @height = file.height
    if @image.update_attributes(image_params)
      file.resize("#{@image.width}x#{@image.height}")
      file.write("public/assets/image_#{@image.id}.#{@image.image_type}")
      redirect_to root_path, notice: 'Image was updated'
    else
      render :edit
    end

  end

  def destroy
    @image = Image.find(params[:id])

    @image.destroy
    redirect_to root_path, notice: 'Image was destroyed'
  end

  private

  def image_params
    params.require(:image).permit(:image, :width, :height, :image_type, :url)
  end
end
